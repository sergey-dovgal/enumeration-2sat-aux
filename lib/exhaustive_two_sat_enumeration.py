from exhaustive_digraph_enumeration import *


def exhaustive_count_cscc(n, m):
    return len(list(cscc_two_sat_iterator(n, m)))


def exhaustive_count_sat(n, m):
    return len(list(satisfiable_two_sat_formula_iterator(n, m)))


def cscc_two_sat_iterator(n, m):
    return filterfalse(lambda formula: not is_cscc_formula(n, formula), two_sat_iterator(n, m))


def satisfiable_two_sat_formula_iterator(n, m):
    return filterfalse(lambda formula: not is_satisfiable(n, formula), two_sat_iterator(n, m))


def is_cscc_formula(n, formula):
    return is_scc_digraph(2 * n, digraph_from_two_sat_formula(formula))


def is_satisfiable(n, formula):
    digraph = digraph_from_two_sat_formula(formula)
    n = 2 * n
    matrix = matrix_from_size_and_digraph(n, digraph)
    walk_matrix = reachability_matrix_from_matrix(matrix)
    for i in range(0, n, 2):
        if walk_matrix[i][i + 1] and walk_matrix[i + 1][i]:
            return False
    return True


def two_sat_iterator(n, m):
    variable_iterator = range(n)
    arc_iterator = combinations(variable_iterator, 2)
    negation_iterator = (True, False)
    negation_pair_iterator = product(negation_iterator, negation_iterator)
    clause_iterator = product(negation_pair_iterator, arc_iterator)
    return combinations(clause_iterator, m)


def digraph_from_two_sat_formula(formula):
    for ((n0, n1), (v0, v1)) in formula:
        int0 = int_from_literal(n0, v0)
        int_neg0 = int_from_literal(not n0, v0)
        int1 = int_from_literal(n1, v1)
        int_neg1 = int_from_literal(not n1, v1)
        yield int_neg0, int1
        yield int_neg1, int0


def int_from_literal(is_negated, variable):
    if is_negated:
        return 2 * variable
    else:
        return 2 * variable + 1
