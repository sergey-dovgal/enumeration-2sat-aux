import sympy as sp
from sympy.polys import ring, QQ
from sympy.polys.ring_series import rs_log, rs_series_inversion


def generating_function_count_scc(n, m):
    _, z, w = ring('z, w', QQ)
    return count_from_egf(n, m, scc_egf(z, w, n + 2))  # Why is this '+ 2' necessary??


def scc_egf(z, w, prec):
    graph = graph_egf(z, w, prec)
    inverse_graph = rs_series_inversion(graph, z, prec)
    return -1 * rs_log(exponential_hadamard_product(graph, inverse_graph, z), z, prec)


def graph_egf(z, w, prec):
    return sum((1 + w)**(n * (n - 1) // 2) * z**n / sp.factorial(n) for n in range(prec))


def digraph_egf(z, w, prec):
    return sum((1 + w)**(n * (n - 1)) * z**n / sp.factorial(n) for n in range(prec))


def exponential_hadamard_product(f0, f1, z):
    f_ring = f0.ring
    res = f_ring.zero
    for n in range(min(f0.degree(z), f1.degree(z))):
        res += extract_coefficient(f0, z, n) * extract_coefficient(f1, z, n) * sp.factorial(n) * z**n
    return res


def count_from_egf(n, m, f):
    f_ring = f.ring
    z, w = f_ring.gens
    return f.coeff(z ** n * w ** m) * sp.factorial(n)


def extract_coefficient(f, x, n):
    f_ring = f.ring
    index_x = f_ring.gens.index(x)
    res = f_ring.zero
    for powers_tuple in f.as_expr_dict():
        if powers_tuple[index_x] == n:
            res[powers_tuple] = res.get(powers_tuple, 0) + f[powers_tuple]
    return res / x**n
