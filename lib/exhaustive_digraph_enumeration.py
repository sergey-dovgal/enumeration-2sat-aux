from itertools import *
import numpy as np


def exhaustive_count_scc(n, m):
    return len(list(scc_digraph_iterator(n, m)))


def scc_digraph_iterator(n, m):
    return filterfalse(lambda digraph: not is_scc_digraph(n, digraph), digraph_iterator(n, m))


def digraph_iterator(n, m):
    vertex_iterator = range(n)
    arc_iterator = permutations(vertex_iterator, 2)
    return combinations(arc_iterator, m)


def is_scc_digraph(n, digraph):
    return is_scc_matrix(matrix_from_size_and_digraph(n, digraph))


def matrix_from_size_and_digraph(n, digraph):
    matrix = np.zeros((n, n), dtype=int)
    for vertex0, vertex1 in digraph:
        matrix[vertex0][vertex1] = 1
    return matrix


def is_scc_matrix(matrix):
    n = len(matrix)
    walk_matrix = reachability_matrix_from_matrix(matrix)
    for i in range(n-1):
        if not walk_matrix[i][i+1]:
            return False
    return bool(walk_matrix[-1][0])


def reachability_matrix_from_matrix(matrix):
    n = len(matrix)
    identity_matrix = np.identity(n, dtype=int)
    return np.linalg.matrix_power(identity_matrix + matrix, n - 1)
