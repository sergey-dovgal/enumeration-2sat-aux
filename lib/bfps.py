import flint
from flint import arb, arb_series

def QQ(x, y):
    """Rational numbers
    """
    return arb(x) / arb(y)

cap = 10

class arb_biseries:
    """A custom python class implementing fast bivariate power series.
    The data is represented as an array of FLINT arb power series.
    """

    def __init__(self, arg = [], direct = False):
        if direct:
            # in this case, all the checks are skipped
            self.seq = arg
        elif isinstance(arg, list):
            if len(arg) >= cap:
                self.seq = [
                    elem if isinstance(elem, arb_series)
                    else arb_series(elem)
                    for elem in arg[:cap]
                ]
            else:
                self.seq = [
                    arg[i] if i < len(arg)
                        and isinstance(arg[i], arb_series)
                    else arb_series(arg[i])
                        if i < len(arg)
                    else arb_series(0)
                    for i in range(cap)
                ]
        elif isinstance(arg, arb_series):
            arb_biseries.__init__(self, [arg])
        elif isinstance(arg, (int, arb)):
            arb_biseries.__init__(self, [arb_series(arg)])
        else:
            # Note that the usage of floats leads to this warning.
            # This is designed intentionally.
            raise NotImplementedError()


    def __repr__(self):
        return str(self.seq)


    def __getitem__(self, idx):
        return self.seq[idx]


    def __setitem__(self, idx, value):
        self.seq[idx] = value


    def __neg__(self):
        return arb_biseries([
            -x_i
            for x_i in self.seq
        ], direct=True)


    def __add__(self, other):
        if isinstance(other, (int, arb, arb_series)):
            return arb_biseries([
                x_i + other if i == 0
                else x_i
                for (x_i, i) in zip(self.seq, range(cap))
            ], direct=True)
        elif isinstance(other, arb_biseries):
            return arb_biseries([
                x_i + y_i
                for (x_i, y_i) in zip(self.seq, other.seq)
            ], direct=True)
        else:
            raise NotImplementedError()

    def __sub__(self, other):
        return self + (-other)


    def __rsub__(self, other):
        return (-self) + other


    def __mul__(self, other):
        if isinstance(other, (arb_series, arb, int)):
            return arb_biseries([
                elem * other
                for elem in self.seq
            ], direct=True)
        elif isinstance(other, arb_biseries):
            # Quadratic algorithm for series multiplication.
            # May be replaced by Karatsuba later.
            return arb_biseries([
                sum([
                    self.seq[k] * other.seq[n-k]
                    for k in range(0, n+1)
                ])
                for n in range(cap)
            ], direct=True)
        else:
            raise NotImplementedError()

    # Addition and multiplication are commutative
    __radd__ = __add__

    __rmul__ = __mul__


    def inv(self):
        result = arb_biseries(0)
        result[0] = arb(1) / self.seq[0]
        n = cap

        # Maybe Newton's iteration gives even better result and the
        # contact order at n steps is 2^2^n, yielding O(log log n) operations
        # instead of just O(log n)?
        # We can test this later for an eventual speed-up.
        # See the book of Bergeron, Labelle, Leroux for further reference.
        # It is weird that van der Hooven's paper plainly suggests
        # O(log n) operations without any further discussion.
        while n >= 1:
            result = 2 * result - self * result * result
            n = n // 2
        return result


    def __rtruediv__(self, other):
        return self.inv() * other


    def __truediv__(self, other):
        if isinstance(other, (int, arb, arb_series)):
            if isinstance(other, (int, arb)):
               return self * (1 / arb_series(other))
            else:
               return self * (1 / other)
        elif isinstance(other, arb_biseries):
            return self * other.inv()
        else:
            raise NotImplementedError()


    def pdiff(self, outer=True):
        """FPS pointed derivative with respect to outer or inner variable.
        Pointed derivative denotes z f'(z).
        Note that plain derivative has some technical difficulties because
        it requires diminishing the cap of the series.
        """
        if outer:
            return arb_biseries([
                elem * n
                for n, elem in zip(range(cap), self.seq)
            ])
        else:
            # currently, we don't need this functionality, but
            # it can be easily implemented and tested.
            raise NotImplementedError()


    def pint(self, const=arb_series(0), outer=True):
        """FPS pointed integral with respect to outer or inner variable.
        Pointed integral denotes Int f(z)/z dz.
        This operation is the reverse of the pointed derivative operation z f'(z).

        The constant term should be provided as a separate argument, but
        assumed to be zero by default.
        It is assumed that the constant term of f(z) is zero, but not asserted.
        The constant term of f(z) is effectively ignored.
        """
        return arb_biseries([
            const if n == 0
            else
                elem * QQ(1, n)
            for n, elem in zip(range(cap), self.seq)
        ])


    def log(self):
        """
        The following fast algorithm is used:
            log f = log f[0] + int[f' / f]
        """
        return arb_biseries.pint(
            self.pdiff() / self,
            self[0].log()
        )

    def exp(self):
        """
        Newton's iteration is used for g(z) = exp(f(z)):
            g[0] = exp(f[0])
            g := g - (log g - f) * g
        It is assumed that the constant term is zero.
        """
        result = arb_biseries(self[0].exp())
        n = cap
        while n >= 1:
            # the same concern as in the implementation of "inv".
            # how many iterations do we actually need?
            result = result - (result.log() - self) * result
            n = n // 2
        return result


    def sqrt(self):
        """Square root of a series.
        Advantage is that the square root is more numerically stable
        than the exp-log iteration.
        """
        result = arb_biseries(self[0].sqrt())
        n = cap
        while n >= 1:
            result = result / 2 + self / result / 2
            n = n // 2
        return result

    # If the exponent is a rational number 1/m, we can apply Newton's iteration
    # result := result * (1 - 1/m) + self / (m * result ** (m-1))
    # Then, exponents p/q are obtained by taking fast powers.
    # It can be implemented by using fmpq rationals.


    def pow(self, other):
        """Integer, rational and real exponents.
        f**n = exp(n * log(f))
        """
        if isinstance(other, int):
            # Fast exponentiation in the case when the exponent is an integer

            if other > 0:
                n = other
            else:
                n = -other

            result = arb_biseries(1)
            if n == 0:
                return result

            running = arb_biseries(self.seq)

            while(n >= 1):
                if n % 2 == 1:
                    result = result * running
                running = running * running
                n = n // 2
            if other > 0:
                return result
            else:
                return result.inv()

        if isinstance(other, arb):
            # Exp-log transform.
            # Important: this algorithm is not numerically stable!
            return (self.log() * other).exp()

        if isinstance(other, float):
            if other * 2 != int(other * 2):
                raise NotImplementedError("Please convert the exponent into arb.")
            else:
                n = int(other * 2)
                return self.sqrt() ** n

        if not isinstance(other, (int, arb, float)):
            raise NotImplementedError()

    __pow__ = pow
