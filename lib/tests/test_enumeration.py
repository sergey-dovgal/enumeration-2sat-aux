import unittest
from generating_function_two_sat_enumeration import *
from exhaustive_two_sat_enumeration import *


class TestEnumeration(unittest.TestCase):

    max_N = 4
    max_M = 7

    def max_n_clauses(self, n):
        return min(2 * n * (n - 1) + 1, self.max_M)

    def test_count_scc(self):
        for n in range(1, self.max_N+1):
            for m in range(self.max_n_clauses(n)+1):
                print(f'count_scc: n={n}, m={m}')
                self.assertEqual(generating_function_count_scc(n, m), exhaustive_count_scc(n, m))

    def test_count_cscc(self):
        for n in range(1, self.max_N+1):
            for m in range(self.max_n_clauses(n)+1):
                print(f'count_cscc: n={n}, m={m}')
                self.assertEqual(exhaustive_count_cscc(n, m), generating_function_count_cscc(n, m))

    def test_count_sat(self):
        for n in range(1, self.max_N+1):
            for m in range(self.max_n_clauses(n)+1):
                print(f'count_sat: n={n}, m={m}')
                self.assertEqual(exhaustive_count_sat(n, m), generating_function_count_sat(n, m))

    def test_second_count_sat(self):
        for n in range(1, self.max_N+1):
            for m in range(self.max_n_clauses(n)+1):
                print(f'count_sat_second_formula: n={n}, m={m}')
                self.assertEqual(generating_function_count_sat(n, m), second_generating_function_count_sat(n, m))


if __name__ == '__main__':
    unittest.main()
