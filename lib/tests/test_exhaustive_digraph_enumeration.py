import unittest
from exhaustive_digraph_enumeration import *
from math import comb as binomial

digraph0 = ((0, 1), (1, 2), (2, 0))
matrix0 = np.array([[0, 1, 0], [0, 0, 1], [1, 0, 0]])
digraph1 = ((0, 0), (0, 1), (1, 0), (1, 1), (2, 2), (2, 3), (3, 2), (3, 3))
matrix1 = np.array([[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]])


def are_equal_matrices(m0, m1):
    return m0.shape == m1.shape and (m0 == m1).all()


def digraph_number(n, m):
    return binomial(n * (n - 1), m)


def edge_number_from_digraph(digraph):
    n = size_from_digraph(digraph)
    matrix = matrix_from_size_and_digraph(n, digraph)
    return sum(sum(matrix))


def count_scc_digraphs_by_vertices(n):
    if n == 0:
        return 1
    return sum(exhaustive_count_scc(n, m) for m in range(n * (n - 1) + 1))


def size_from_digraph(digraph):
    return max(max(v0, v1) for v0, v1 in digraph) + 1


class TestStrongDigraph(unittest.TestCase):

    def test_is_scc_matrix(self):
        self.assertTrue(is_scc_matrix(matrix0))
        self.assertFalse(is_scc_matrix(matrix1))

    def test_matrix_from_size_and_digraph(self):
        self.assertTrue(are_equal_matrices(matrix_from_size_and_digraph(3, digraph0), matrix0))
        self.assertTrue(are_equal_matrices(matrix_from_size_and_digraph(4, digraph1), matrix1))
        self.assertFalse(are_equal_matrices(matrix_from_size_and_digraph(4, digraph0), matrix0))

    def test_is_scc_digraph(self):
        self.assertTrue(is_scc_digraph(3, digraph0))
        self.assertFalse(is_scc_digraph(4, digraph1))

    def test_digraph_iterator(self):
        for n, m in [(2, 3), (4, 4), (5, 2), (4, 3)]:
            self.assertEqual(len(list(digraph_iterator(n, m))), digraph_number(n, m))

    def test_size_from_digraph(self):
        self.assertEqual(size_from_digraph(digraph0), 3)
        self.assertEqual(size_from_digraph(digraph1), 4)

    def test_edge_number_from_digraph(self):
        self.assertEqual(edge_number_from_digraph(digraph0), 3)
        self.assertEqual(edge_number_from_digraph(digraph1), 8)

    def test_edge_number_in_scc_digraph_iterator(self):
        for n, m in [(2, 3), (4, 4), (5, 2), (4, 3)]:
            for digraph in scc_digraph_iterator(n, m):
                self.assertEqual(edge_number_from_digraph(digraph), m)

    def test_count_scc_digraphs(self):
        digraph_numbers = [1, 1, 1, 18, 1606]
        for n in range(5):
            self.assertEqual(count_scc_digraphs_by_vertices(n), digraph_numbers[n])


if __name__ == '__main__':
    unittest.main()
