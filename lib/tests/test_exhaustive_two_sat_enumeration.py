import unittest
from exhaustive_two_sat_enumeration import *
from math import comb as binomial


formula0 = (((False, True), (1, 2)), ((True, True), (0, 2)), ((False, False), (0, 1)))
formula1 = (((True, True), (0, 1)), ((False, True), (0, 1)), ((True, False), (0, 1)), ((False, False), (0, 1)))


class TestExhaustiveTwoSatEnumeration(unittest.TestCase):

    def test_int_from_literal(self):
        n = 5
        variables = range(n)
        vertices = [int_from_literal(True, v) for v in variables] + [int_from_literal(False, v) for v in variables]
        vertices.sort()
        self.assertEqual(vertices, list(range(2 * n)))

    def test_digraph_from_two_sat_formula(self):
        digraph0 = digraph_from_two_sat_formula(formula0)
        self.assertEqual(2 * len(formula0), len(list(digraph0)))
        digraph1 = digraph_from_two_sat_formula(formula1)
        self.assertEqual(2 * len(formula1), len(list(digraph1)))

    def test_two_sat_iterator(self):
        for n, m in [(3, 4), (5, 2), (2, 1), (3, 2)]:
            self.assertEqual(len(list(two_sat_iterator(n, m))), binomial(2 * n * (n - 1), m))

    def test_exhaustive_count_cscc_formulas(self):
        self.assertEqual(exhaustive_count_cscc(2, 4), 1)

    def test_is_satisfiable(self):
        self.assertTrue(is_satisfiable(3, formula0))
        self.assertFalse(is_satisfiable(2, formula1))

    def test_exhaustive_count_satisfiable_two_sat_formulas(self):
        self.assertEqual(exhaustive_count_sat(2, 4), 0)
        self.assertEqual(exhaustive_count_sat(2, 3), 4)


if __name__ == '__main__':
    unittest.main()
