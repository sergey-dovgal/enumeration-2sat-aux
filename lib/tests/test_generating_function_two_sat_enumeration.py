import unittest
from generating_function_two_sat_enumeration import *
from exhaustive_two_sat_enumeration import *


class TestGeneratingFunctionTwoSatEnumeration(unittest.TestCase):

    def test_count_cscc(self):
        for n in range(1, 5):
            for m in range(2 * n * (n - 1)):
                self.assertEqual(exhaustive_count_cscc(n, m), generating_function_count_cscc(n, m))


if __name__ == '__main__':
    unittest.main()
