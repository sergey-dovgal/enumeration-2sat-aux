from generating_function_digraph_enumeration import *
from exhaustive_two_sat_enumeration import exhaustive_count_cscc, exhaustive_count_sat
from sympy.polys.ring_series import rs_exp, rs_pow
from math import comb as binomial


def generating_function_count_cscc(n, m):
    _, z, w = ring('z, w', QQ)
    return count_from_egf(n, m, cscc_egf(z, w, n + 2))


def generating_function_count_sat(n, m):
    _, z, w = ring('z, w', QQ)
    return count_from_egf(n, m, sat_egf(z, w, n + 2))


def second_generating_function_count_sat(n, m):
    """
    \SAT_{n,m} = 2^n n! [z^n w^m] \gset((1 + w)^{2(n-1)} z, w) \sqrt{G(z,w) \odot_z \frac{1}{G(z,w)}}
    """
    _, z, w = ring('z, w', QQ)
    graph = graph_egf(z, w, n + 2)
    inverse_graph = rs_series_inversion(graph, z, n + 2)
    sqrt_h_graph = rs_sqrt(exponential_hadamard_product(graph, inverse_graph, z), z, n + 2)
    return 2**n * count_from_egf(n, m, set_ggf(z, w, n + 1, m + 1).compose(z, (1 + w)**(2 * (n - 1)) * z)
                                 * sqrt_h_graph)


def cscc_egf(z, w, prec):
    graph = graph_egf(z, w, prec)
    inverse_graph = rs_series_inversion(graph, z, prec)
    digraph = digraph_egf(z, w, prec)
    digraph2 = digraph.compose(z, 2 * z)
    digraph_2 = digraph.compose(z, z / 2)
    hadamard = exponential_hadamard_product(digraph2, digraph_2 * inverse_graph, z)
    return 1/2 * scc_egf(z, w, prec).compose(z, 2 * z) + rs_log(hadamard, z, prec)


def sat_egf(z, w, prec):
    graph = graph_egf(z, w, prec)
    gg = exponential_hadamard_product(graph, rs_series_inversion(graph, z, prec), z)
    set_idgf_2 = set_idgf(z, w, prec, 2 * prec * (prec - 1) + 2).compose(z, 2 * z)
    sat_idgf = graph * exponential_hadamard_product(rs_sqrt(gg, z, prec), set_idgf_2, z)
    digraph2 = digraph_egf(z, w, prec).compose(z, 2 * z)
    return exponential_hadamard_product(sat_idgf, digraph2, z)


def set_idgf(z, w, z_prec, w_prec):
    return sum(rs_pow(1 + w, - n * (n - 1), w, w_prec) * (z / 2) ** n / sp.factorial(n) for n in range(z_prec))


def set_ggf(z, w, z_prec, w_prec):
    return sum(rs_pow(1 + w, - n * (n - 1) // 2, w, w_prec) * z ** n / sp.factorial(n) for n in range(z_prec))


def rs_sqrt(f, z, prec):
    return rs_fractional_power(f, 1/2, z, prec)


def rs_fractional_power(f, power, z, prec):
    return rs_exp(power * rs_log(f, z, prec), z, prec)
