# Supplementary material for enumeration of the satisfiable 2-SAT formulae

This repository contains `Python` code and `IPython` notebooks accompanying the corresponding paper "Exact enumeration of satisfiable 2-SAT formulae" by *Sergey Dovgal, Élie de Panafieu, and Vlady Ravelomanana* available online as an arxiv preprint <https://arxiv.org/abs/2108.08067>.

In the paper, we provide several new generating functions: the generating function of satisfiable 2-CNF with marked patterns and the generating function of contradictory
strongly connected components. These families are expressed in the language of directed
graphs (implication digraphs), but can be also defined using the language of disjunctive
clauses.

A Boolean variable is called *contradictory* if there is a satisfiable subformula in which
this variable can take only the TRUE value, and another satisfiable subformula in which
it can only take the value FALSE. The contradictory components are formulae in which every
variable is contradictory, and they play an important role in the enumeration of
various 2-CNF families.

Currently, no asymptotic toolbox is available for study of these structures, but in the
current repository we propose very high-precision predictions of the limiting shape
of the phase transition, based on our assumption about the full asymptotic expansion
inside the critical window.

The easiest way to access the contents of this repository is through looking at the IPython Notebooks presenting the material in the most friendly fashion.

## IPython Notebooks

The directory `ipynb` contains `IPython` notebooks which are detailed computation reports. The notebooks are executed using `Python 3` and may require installing additional python libraries. It is possible to view the contents of these notebooks without installing any additional software using `nbviewer`. The links are provided separately to each notebook below.

* [`Sequences.ipynb`][Seq]. Enumerating formulae of small sizes using the generating functions
* [`Numerical.ipynb`][Num]. Numerical predictions of the limiting probabilities.
* [`Timing-bivariate.ipynb`][Tim]. Empirical time measurements for
  computing satisfiable formulae.

[Seq]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/enumeration-2sat-aux/-/raw/master/ipynb/Sequences.ipynb
[Num]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/enumeration-2sat-aux/-/raw/master/ipynb/Numerical.ipynb
[Tim]: https://nbviewer.jupyter.org/urls/gitlab.com/sergey-dovgal/enumeration-2sat-aux/-/raw/master/ipynb/Timing-bivariate.ipynb

## Utility library

There are several `Python` modules intended for counting of digraph families, exhaustive enumeration, and symbolic computations. They are all containted in `lib` directory.

* `bfps.py`: a module for fast bivariate formal power series
  manipulation based on FLINT.
* `exhaustive_digraph_enumeration.py`: a module for exhaustive generation and counting of digraph families.
* `exhaustive_two_sat_enumeration.py`: a module for exhaustive generation and counting of 2-sat families.
* `generating_function_digraph_enumeration.py`: a module containing the generating functions
for digraph families.
* `generating_function_two_sat_enumeration.py`: a module containing the generating functions
for 2-sat families.

## Automated testing

In order to launch all the tests, you need to clone the repository, enter the `lib` directory and launch in the command line
```
python3 -m unittest tests/*.py
```
It is also possible to test separate modules or run isolated tests:
```
python3 -m unittest tests/test_enumeration.py
```

## Feedback

This code has been jointly written by Sergey Dovgal and Élie de Panafieu
